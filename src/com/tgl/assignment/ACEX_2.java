package com.tgl.assignment;

//Write a method that prints the positive odd numbers less than 20
public class ACEX_2 {
	public static void main(String[] args) {
		oddNumber(20);
		String oddret = oddNum(20);
		System.out.println("**** 2. OUTPUT ****-->"+oddret);
	}

	public static void oddNumber(int odd) {
		int x = 0;
		System.out.println("*** 1. Output ***");
		for (int i = 1; i < odd; i++) {
			x = i % 2;
			if (x != 0) {
				System.out.println(i);
			}
		}
	}
	
	public static String oddNum(int odd) {
		int x = 0;
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < odd; i++) {
			x = i % 2;
			if (x != 0) {
				//ret = ret.concat(Integer.toString(i).concat(" "));
				sb.append(Integer.toString(i));
				sb.append(" ");
			}
		}
		return sb.toString();
	}
}
