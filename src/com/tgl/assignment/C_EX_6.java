//Write a method that returns the last element of a string array
//String [] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"}
//System.out.println(lastElement(breakfast));

package com.tgl.assignment;

public class C_EX_6 {
	public static void main(String[] args)
	{
		String[] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		System.out.println(lastElement(breakfast));
	}
	public static String lastElement(String[] Str1)
	{
		int len=Str1.length;
		return Str1[len-1];
	}
}
