package com.tgl.assignment;
// input string
// output : 第一個字大寫->True, 其它為->False

import java.util.Scanner;

public final class EX_7_1 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
				
		System.out.println("Please input : ");
		String str = input.next();
		boolean b = true;
		char c = str.charAt(0);
		if( Character.isUpperCase(c))
			b = true;
		else
			b = false;
		
		System.out.print(b); ;    //System.out.printf("%b")
		
	}
	
}
