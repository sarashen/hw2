//Write a method that tests to see if an array is palindromic,
// i.e. the elements are the same when reversed.

package com.tgl.assignment;

import java.util.Arrays;

public  class C_EX_8 {
	public static void main(String[] args) 
	{
		String[] palindromic= {"Sausage","Eggs","Beans","Beans","Eggs","Sausage"};
		String[] breakfast = {"Sausage","Eggs","Beans","Bacon","Tomatoes","Mushrooms"};
		
		boolean b = true;
		b=func1(palindromic);
	//	System.out.print(palindromic);
		System.out.print(Arrays.toString(palindromic));
		System.out.println("-->"+b);
		
		b = func1(breakfast);
//		System.out.print(breakfast);
		System.out.print(Arrays.toString(breakfast));
		System.out.println("-->"+b);
	}		
	public static boolean func1(String[] a) 
	{	
		if ( a == null)
			return false;
		
		int len = a.length;
		
		int lenHelf = len / 2;
		for(int i=0;i<lenHelf;i++)
		{
			if (a[i] != a[lenHelf*2-i-1])
				return false;
		}
		//char[] arr = a.toCharArray();
		return true;
		
	}
}

