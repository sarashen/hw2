package com.tgl.assignment;
// Given a 32-bit signed integer, reverse digits of an integer

import java.util.Scanner;

public final class EX_3_1 {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Input no:");
		int num = input.nextInt();

		int num1 = num % 10;
		if (num1 == 0) {
			num /= 10;
		}

		String str = Integer.toString(num);
		System.out.println("Reverse Input no:");

		int x = str.length() - 1;
		for (int i = x; i >= 0; i--)
			System.out.print(str.charAt(i));
	}
}
