package com.tgl.assignment;

//Write a method that prints the square numbers up to 100
public class ACEX_3 {
	public static void main(String[] args) {
		Squares(100);
	//	Squares(1000);
	}

	public static void Squares(int squmax) {
		int res = 0;
		System.out.println("***Output Max :"+squmax );
		for (int i = 1; res <= squmax; i++) {
			res = (int) Math.pow(i, 2);
			if (res <= squmax) {
				System.out.println(res);
			}
		}

	}
}
