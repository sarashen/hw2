//Given an array of integers, 
//return indices of the two numbers such that they add up to a specific target

package com.tgl.assignment;

public class EX_11_1 {
	public static void main(String[] args) {
	
		int num[] = {2,7,11,15};
//		int[] num = new int[4] {2,7,11,15};
		int target = 17;
		
		for (int i=0;i<num.length-1;i++) {
			for (int j=i+1;j<num.length;j++) {
				if (target == num[i]+num[j] )
						System.out.println("return:"+i+","+j);
			}			
		}	
	}

}
