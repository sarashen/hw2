package com.tgl.assignment;
//Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string. 
//If the last word does not exist, return 0.
// ERROR 

import java.util.Scanner;

public final class EX_6_1 {
	public static void main(String[] args) {
	//	String str = "Happy every day";
		Scanner input = new Scanner(System.in);
		
		System.out.print("input:");	
		String str=input.nextLine();
		String[] arr = str.split(" ");
		System.out.println("last word length:"+arr[arr.length-1].length());
		System.out.println("last word is :"+arr[arr.length-1]);
	}

}
