package com.tgl.assignemt1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
/* import java.text.DecimalFormat; */ 
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileIO {
	// ?? 為何此LIST要宣告為static
	private static List<Employee> emp = new ArrayList<>();

	private static long id = 1;
/*	EnumField field = Field.values.ID; */ 

	public static void main(String[] arg) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("D://TEMP/hw.txt"));

		} catch (FileNotFoundException e) {
			System.out.println(e);
			return;
		}
		String line, ename, name, ext, email;
		int high, weigh;
		float bmi, high1, weigh1,bmi1;
		DecimalFormat df = new DecimalFormat("##.00"); 
		
	 
		try {
			while ((line = br.readLine()) != null) {
				String[] se = line.split(" ");
				System.out.println(line);
				high = Integer.parseInt(se[0]);
				weigh = Integer.parseInt(se[1]);
				ename = se[2];
				name = se[3];
				ext = se[4];
				email = se[5];
				high1 =  (float) high / 100;
				weigh1 = (float) weigh;
				bmi =  (float) (weigh1 / (high1 * high1 ));
				bmi1 = (float) Double.parseDouble(df.format(bmi));   
				Employee s = new Employee(id, high, weigh, ename, name, ext, email, bmi1);
				emp.add(s);
				id++;
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if ( br != null ) {
				try {
					br.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
						
		for (int i = 0; i < emp.size(); i++) {
			Employee s1 = (Employee) emp.get(i);
//			System.out.println("output->" + s1);
			System.out.println("INPUT SOURCE DATA->" + s1.toString());
		}
		
		Scanner sc = new Scanner(System.in);

		int inqField, compType;
		String inqKey;
		System.out.println("<<< Please input Function 1:inquiry 2:sort 3:add rec 4:delete rec5:Max,Min (0:end)--> ");
		int  inpFun = sc.nextInt();

		EmpFunction empFunction = new EmpFunction();

		while (inpFun != 0) {
			switch (inpFun) {
			case 1:  /* inquery function */
				System.out.println("*** Please input 0->ID,1->eName,2->Name,3->ext,4->e-mail");
				inqField = sc.nextInt();
				System.out.println("*** Please inquery value:");
				inqKey = sc.next();
				// String ret = inqEmp(inqField, inqKey);
				String ret = empFunction.inqEmp(emp, inqField, inqKey);
				if (ret == null) {
					System.out.println("inqKey-->" + inqKey);
				} else {
					System.out.println("Other Information==>" + ret);
				}
				break;
			case 2:   /* Sort  function */
				System.out.println("*** Sort by : 0->ID, 1->high,2->weigh,3->English Name,4->extension,5->e-mail,6->BMI");
				inqField = sc.nextInt();
				if (empFunction.sortEmp(emp, inqField)) {
					// ?? 為何下列for 不可寫在sortEmp 中?
					for (Employee st : emp) {
						System.out.println(st);
					}
				}
				break;
			case 3:   /* Insert  record */
				int empSize = empFunction.insertEmp(emp, id) ;
				System.out.println("*** Employee size :"+empSize);
				id++;
				for (int i = 0; i < emp.size(); i++) {
					Employee s1 = (Employee) emp.get(i);

					System.out.println("ADD after->" + s1.toString());
				}
				
				break;
				
			case 4:   /* Delete  record */
				System.out.println("*** Please input 0->id,1->English Name,2->Extension");
				inqField = sc.nextInt();
				System.out.println("***Please input Key Value:");
				inqKey = sc.next();
				empSize = empFunction.deleteEmp(emp, inqField,inqKey) ;
				System.out.println("*** kill After--Employee size :"+empSize);
				
				for (int i = 0; i < emp.size(); i++) {
					Employee s1 = (Employee) emp.get(i);

					System.out.println("kill after->" + s1.toString());
				}
				
				break;
			case 5:
				System.out.println("*** Please input 0->high,1->weigh,2->BMI");
				inqField = sc.nextInt();
				System.out.println("*** 0:Min 1:max");
				compType = sc.nextInt();
				if (compType == 0) {
					ret = empFunction.inqMin(emp, inqField);
					System.out.println("Min-->" + ret);
				}
				else {
				    ret = empFunction.inqMax(emp, inqField);
				    System.out.println("Man-->" + ret);
				}
				break;
			default:
				System.out.println("Input error ~~");
				break;
			} // end switch
			System.out.println("<<< Please input Function 1:inquiry 2:sort 3:add rec 4:delete rec5:Max,Min (0:end)--> ");
			inpFun = sc.nextInt();
		} // end while
		System.out.println(" ~~ Welcome ~~ ");
	} // end main
} // end class