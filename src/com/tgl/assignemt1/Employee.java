package com.tgl.assignemt1;

import java.util.Comparator;
import java.util.List;
public class Employee {
	// ?? Integer 與 int 的差別
	private long id;
	private int high;
	private int weight;
	private String ename;
	private String name;
	private String ext;
	private String email;
	private float bmi;
	
	public Employee(long id, int high,int weight,String ename,String name,String ext,String email,float bmi)
	{
		this.id = id;
		this.high = high;
		this.weight = weight;
		this.ename=ename;
		this.name = name;
		this.ext = ext;
		this.email = email;
		this.bmi = bmi;
	}
	public float getBmi() {
		return bmi;
	}

	public void setebmi(float bmi) {
		this.bmi = bmi ;
	}
	
	public String getEmail() {
		return email;
	}

	public void setemail(String email) {
		this.email = email;
	}
	
	public String getExt() {
		return ext;
	}

	public void setext(String ext) {
		this.ext = ext;
	}
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public int getHigh() {
		return high;
	}

	public void setHigh(int high) {
		this.high = high;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.ename = name;
	}

	@Override
	public String toString() {
		return "Employee [id="+id+", high="+high + ", weight=" + weight + ", ename=" + ename +  ", name=" + name +" , ext="+ext +" ,email="+ email +",bmi="+bmi+" ]";
	}
	
// ?? 為何此處不能有Override
//	@Override
//	public int compareTo(Employee ep ) {
//		int comparhigh = ((Employee)ep).getHigh();
//		return this.high - comparhigh;
//	}
	
	public static Comparator<Employee> IdComparator = new Comparator<Employee>() 
	{
		public int compare(Employee obj1,Employee obj2) {
			long s1 = obj1.getId();
			long s2 = obj2.getId();
			
			int i = 0;
			if (s1 > s2) {
				i = 1;
			}
			else
			{	
				i = -1;
			}
			return i;
		}
	};
	
	public static Comparator<Employee> HighComparator = new Comparator<Employee>() 
	{
		public int compare(Employee obj1,Employee obj2) {
			int s1 = obj1.getHigh();
			int s2 = obj2.getHigh();
			
			return s1-s2;
		}
	};
	//?? 為何這有;號	
	public static Comparator<Employee> WeightComparator = new Comparator<Employee>() 
	{
		public int compare(Employee obj1,Employee obj2) {
			int s1 = obj1.getWeight();
			int s2 = obj2.getWeight();
			
			return s1-s2;
		}
	}; 
	public static Comparator<Employee> EnameComparator = new Comparator<Employee>() 
	{
		public int compare(Employee obj1,Employee obj2) {
			String s1 = obj1.getEname();
			String s2 = obj2.getEname();
			
			return s1.compareTo(s2);
		}
	};
	public static Comparator<Employee> ExtComparator = new Comparator<Employee>() 
	{
		public int compare(Employee obj1,Employee obj2) {
			String s1 = obj1.getExt();
			String s2 = obj2.getExt();
			
			return s1.compareTo(s2);
		}
	};
	public static Comparator<Employee> EmailComparator = new Comparator<Employee>() 
	{
		public int compare(Employee obj1,Employee obj2) {
			String s1 = obj1.getEmail();
			String s2 = obj2.getEmail();
			
			return s1.compareTo(s2);
		}
	};
	public static Comparator<Employee> BmiComparator = new Comparator<Employee>() 
	{
		public int compare(Employee obj1,Employee obj2) {
			float s1 = obj1.getBmi();
			float s2 = obj2.getBmi();
			int i = 0;
			if (s1 > s2) {
				i = 1;
			}
			else
			{	
					i = -1;
			}
			return i;
		}
	};

	
}
