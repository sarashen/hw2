package com.tgl.assignemt1;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class EmpFunction {
/*** 1 : Inquiry ***/	
	public String inqEmp(List<Employee> emp, int iField, String iKey) {
		// ?? 為何下列顯示內容不是 0
		// System.out.printf("TEST: "+FieldKey.ENAME);
		int key = Integer.parseInt(iKey);
		switch (iField) {
		case 0:  /* ID */
			for (int i = 0; i < emp.size(); i++) {
				if (emp.get(i).getId() == key) {
					return emp.get(i).toString();
				}
			}
			return null;
		case 1:  /* english name */
			for (int i = 0; i < emp.size(); i++) {
				if (emp.get(i).getEname().equals(iKey)) {
					// System.out.println("inqKey Aya-Wang->" + emp.get(i).toString());
					return emp.get(i).toString();
				}
			}
			return null;
		case 2:  /* name */
			for (int i = 0; i < emp.size(); i++) {
				if (emp.get(i).getName().equals(iKey)) {
					return (emp.get(i).toString());
				}
			}
			return null;
		case 3:  /* ext */
			for (int i = 0; i < emp.size(); i++) {
				if (emp.get(i).getExt().equals(iKey)) {
					return (emp.get(i).toString());
				}
			}
			return null;
		case 4:   /* e-mail */
			for (int i = 0; i < emp.size(); i++) {
				if (emp.get(i).getEmail().equals(iKey)) {
					return (emp.get(i).toString());
				}
			}
			return null;
		default:
			return null;
		}// end switch
	}
/*** 3 : Insert data ***/	
	public int insertEmp(List<Employee> emp, long id) {
		int high1,weigh1;
		String ename1,name1,ext1,email1;
		float high2,weigh2,bmi1;
		
		Scanner sc1 = new Scanner(System.in);
		DecimalFormat df1 = new DecimalFormat("##.00");

		System.out.println("Please input : "+id);
		System.out.println("Please input : High -> ");
		high1 = sc1.nextInt(); 
		System.out.println("Please input : Weigh -> ");
		weigh1 = sc1.nextInt(); 
		System.out.println("Please input : English Name -> ");
		ename1 = sc1.next();
		System.out.println("Please input : Chinese Name -> ");
		name1 = sc1.next();
		System.out.println("Please input : Extension -> ");
		ext1 = sc1.next();
		System.out.println("Please input : E-mail -> ");
		email1 = sc1.next();
		
		sc1.close();
		
		high2 =  (float) high1 / 100;
		weigh2 = (float) weigh1;
		bmi1 =  (float) (weigh2 / (high2 * high2 ));
		bmi1 = (float) Double.parseDouble(df1.format(bmi1));
		Employee s = new Employee(id, weigh1, high1, ename1, name1, ext1, email1, bmi1);
		emp.add(s);
		return emp.size();
				
	}// end insertEmp
	
	/*** 4 : Delete  data ***/	
	public int deleteEmp( List<Employee> emp, int inqField, String inqKey) {
		long iD1;
		switch( inqField ) {
/*		case EnumField.id :          BY ID */
		case 0 :
			iD1 = Long.parseLong(inqKey);
			for (int i = 0; i < emp.size(); i++) {
				if (iD1 == emp.get(i).getId()) {
					emp.remove(i); 
				    break;
				}
			}
			return emp.size();
			
		case 1:   /*  BY English Name */ 
			System.out.printf("KELETE:"+inqKey);
			
			for (int i = 0; i < emp.size(); i++) {
				if ( emp.get(i).getEname().equals(inqKey)) {
					emp.remove(i); 
				    break;
				}
			}
			
		case 2:   /*  BY extension */ 
			for (int i = 0; i < emp.size(); i++) {
				if (emp.get(i).getExt().equals(inqKey)) {
					emp.remove(i); 
				    break;
				}
			}
			
		default : break;
		}//end switch
		return emp.size();
		
	} // deleteEmp method		
//-----5-1 Max---------------------------------------------------------------	
	public  String inqMax( List<Employee> emp,int iField) {
		int ind = 0;
		switch (iField) {
		case 0 :
			int high1;
			int high = emp.get(0).getHigh();
			for (int i = 1; i < emp.size(); i++) {
				high1 = emp.get(i).getHigh();
				if (high < high1) {
					high = high1;
					ind = i;
				}
			}
			break;

		case 1:
			int weight1;
			int weight = emp.get(0).getWeight();
			for (int i = 1; i < emp.size(); i++) {
				weight1 = emp.get(i).getWeight();
				if (weight < weight1) {
					weight = weight1;
					ind = i;
				}
			}
			break;
		case 2:
			float bmi1;
			float bmi  = emp.get(0).getBmi();
			for (int i = 1; i < emp.size(); i++) {
				bmi1 = emp.get(i).getBmi();
				if (bmi < bmi1) {
					bmi = bmi1;
					ind = i;
				}
			}
			break;
		}
		return emp.get(ind).toString();
	}
	/*  ---- 5-2----- Min  -------------------------------------------------*/
	public  String inqMin(List<Employee> emp, int iField) {
		int ind = 0;
		switch (iField) {
		case 0:
			int high1;
			int high = emp.get(0).getHigh();
			for (int i = 1; i < emp.size(); i++) {
				high1 = emp.get(i).getHigh();
				if (high > high1) {
					high = high1;
					ind = i;
				}
			}
			break;

		case 1:
			int weight1;
			int weight = emp.get(0).getWeight();
			for (int i = 1; i < emp.size(); i++) {
				weight1 = emp.get(i).getWeight();
				if (weight > weight1) {
					weight = weight1;
					ind = i;
				}
			}
			break;
			
		case 2:
			float bmi1;
			float bmi = emp.get(0).getBmi();
			for (int i = 1; i < emp.size(); i++) {
				bmi1 = emp.get(i).getBmi();
				if (bmi > bmi1) {
					bmi = bmi1;
					ind = i;
				}
			}
			break;

		}
		return emp.get(ind).toString();
	}
	
/* 2 - sort function --------------------------------------------------------------*/ 
	public static Boolean sortEmp(List <Employee> emp, int iField) {
		switch (iField) {
		case 0:
			Collections.sort(emp, Employee.IdComparator);
			break;
		case 1:
			Collections.sort(emp, Employee.HighComparator);
			break;
		case 2:
			Collections.sort(emp, Employee.WeightComparator);
			break;
		case 3:
			Collections.sort(emp, Employee.EnameComparator);
			break;
		case 4:
			Collections.sort(emp, Employee.ExtComparator);
			break;
		case 5:
			Collections.sort(emp, Employee.EmailComparator);
			break;
		case 6:
			Collections.sort(emp, Employee.BmiComparator);
			break;
		default:
			System.out.println("Input Error:");
			return false;

		}// end switch
		return true;
	}
} // class EmpFunction
