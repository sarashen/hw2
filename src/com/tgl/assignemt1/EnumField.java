package com.tgl.assignemt1;

import java.lang.Enum;

public enum EnumField {
	ID, 
	HIGH,
	WEIGH,
	ENAME,
	NAME,
	EXTENSION,
	EMAIL,
	BMI
}
